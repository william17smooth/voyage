extends CanvasLayer
export (Texture) var item_objetivo

func _ready():
	get_node("item").texture = item_objetivo
	get_node("item").hide()
	get_node("perdiste/reintentar").connect("focus_entered",self,"opciones")
	get_node("perdiste/salir").connect("focus_entered",self,"opciones")
	get_node("pausa/continuar").connect("focus_entered",self,"opciones")
	get_node("pausa/salir").connect("focus_entered",self,"opciones")

func _process(delta):
	if Input.is_action_just_pressed("pausa"):
		get_tree().paused = not get_tree().paused
		pausa()

func pausa():
	$pausa.visible = get_tree().paused
	$pausa/fullscreen.pressed = OS.window_fullscreen
	if get_tree().paused:
		var volume = range_lerp(Game.volume,Game.MIN_VOL,Game.MAX_VOL,0,80)
		$pausa/volumen.value = volume
		$pausa.rect_position=Vector2.ZERO
		$pausa/continuar.grab_focus()
		get_tree().current_scene.bgm.set_bus("pause")
		return
	$pausa.rect_position=Vector2(1025,0)
	get_tree().current_scene.bgm.set_bus("Master")
	
func objetivo():
	$AnimationPlayer.play("objetivo")
		
func opciones():
	get_node("opciones").play()
#	get_tree().current_scene.bgm.bus = "Master"


func _on_fullscreen_toggled(button_pressed):
	OS.window_fullscreen = button_pressed
