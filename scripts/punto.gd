extends StaticBody2D

export (NodePath) var item_asociado

export (Game.mejoras) var mejora


func _ready():
	pass




func _on_buster_area_entered(area):

	if area.name == "player":
		if mejora == Game.mejoras.velocidad:
			var player = get_tree().get_nodes_in_group("player")[0]
			player.velocidad = Player.velocidad_base * 2
			mejora = Game.mejoras.ninguna
			Sfx.play("booster_velocidad")
			get_node(item_asociado).queue_free()
		
		if mejora == Game.mejoras.objetivo:
			get_tree().get_nodes_in_group("objetivo")[0].hide()
			get_tree().get_nodes_in_group("player")[0].tengo_item = true
			Sfx.play("objetivo")
			get_tree().current_scene.get_node("gui").objetivo()
			mejora = Game.mejoras.ninguna
		
		if mejora == Game.mejoras.oxigeno:
			get_tree().current_scene.oxigeno=min(get_tree().current_scene.oxigeno + 30, 100)
			Sfx.play("recarga_oxigeno")
			mejora = Game.mejoras.ninguna
			get_node(item_asociado).queue_free()
		
		if mejora == Game.mejoras.sig_nivel and get_tree().get_nodes_in_group("player")[0].tengo_item:
			
				
			get_tree().current_scene.bgm.stop()
			get_tree().current_scene.get_node("gui/oxigeno2").stop()
			if get_tree().current_scene.name!="nivel5":
				$win.play()
			
			get_tree().current_scene.siguiente()
			
