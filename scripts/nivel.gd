extends Node2D
export var perdida_oxigeno = 2
var oxigeno = 100
export (PackedScene) var siguiente_nivel
export (AudioStream) var musica
var bgm = AudioStreamPlayer.new()

func _ready():
	bgm.name="bgm"
	bgm.stream = musica
	add_child(bgm)
	bgm.pause_mode=Node.PAUSE_MODE_PROCESS
	bgm.play()

func _physics_process(delta):
	oxigeno-=delta*perdida_oxigeno
	$gui/oxigeno.value=oxigeno
	$gui/oxigeno.tint_progress.g = oxigeno/100 
	$gui/oxigeno.tint_progress.b = oxigeno/100
	if oxigeno < 35 and not $gui/oxigeno2.is_playing():
		$gui/oxigeno2.play()
	if oxigeno<=0:
		
		set_physics_process(false)
		
		$gui/perdiste.mostrar()

func siguiente():
	set_physics_process(false)
	if name == "nivel5":
		$gui/AnimationPlayer.play("fin")
	else:
		$gui/AnimationPlayer.play("close")
	
	yield($gui/AnimationPlayer,"animation_finished")
	get_tree().change_scene_to(siguiente_nivel)
