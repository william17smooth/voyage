extends Control


func _ready():
	pass

func mostrar():
	$lose.play()
	rect_position = Vector2.ZERO
	get_tree().current_scene.bgm.stop()
	get_parent().get_node("oxigeno2").stop()
	get_tree().paused=true
	yield($lose,"finished")
	$reintentar.grab_focus()
	


func _on_reintentar_button_up():
	get_parent().get_node("seleccion").play()
	get_parent().get_node("AnimationPlayer").play("close")
	yield(get_parent().get_node("AnimationPlayer"),"animation_finished")
	get_tree().paused=false
	get_tree().reload_current_scene()



func _on_salir_button_up():
	get_parent().get_node("seleccion").play()
	get_parent().get_node("AnimationPlayer").play("close")
	yield(get_parent().get_node("AnimationPlayer"),"animation_finished")
	get_tree().paused=false
	get_tree().change_scene("res://escenas/menu.tscn")


