extends Control


func _ready():
	pass


func _on_continuar_button_up():
	get_parent().get_node("seleccion").play()
	get_tree().paused = false
	visible = false
	get_tree().current_scene.bgm.set_bus("Master")


func _on_salir_button_up():
	get_parent().get_node("seleccion").play()
	get_parent().get_node("AnimationPlayer").play("close")
	yield(get_parent().get_node("AnimationPlayer"),"animation_finished")
	get_tree().paused=false
	get_tree().change_scene("res://escenas/menu.tscn")


	


func _on_volumen_value_changed(value):
	var volume = range_lerp(value,0,80,Game.MIN_VOL,Game.MAX_VOL)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), volume)
	Game.volume = volume
	
