extends Control


func _ready():
	$fullscreen.pressed = OS.window_fullscreen
	$jugar.connect("focus_entered",self,"seleccion")
	$creditos.connect("focus_entered",self,"seleccion")
	$salir.connect("focus_entered",self,"seleccion")
	$jugar.grab_focus()


func _on_video_finished():
	$video.play()


func _on_jugar_button_up():
	$bgm.stop()
	$seleccion.play()
	$anim.play("jugar")
	yield($anim,"animation_finished")
	get_tree().change_scene("res://escenas/intro.tscn")


func _on_creditos_button_up():
	$bgm.stop()
	$seleccion.play()
	$anim.play("jugar")
	yield($anim,"animation_finished")
	get_tree().change_scene("res://escenas/creditos.tscn")


func _on_salir_button_up():
	$bgm.stop()
	$seleccion.play()
	$anim.play("jugar")
	yield($anim,"animation_finished")
	get_tree().quit()


func seleccion():
	$opciones.play()


func _on_fullscreen_toggled(button_pressed):
	OS.window_fullscreen = button_pressed
