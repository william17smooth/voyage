extends Node2D

export (PackedScene) var siguiente

func _ready():
	$siguiente.grab_focus()


func _on_siguiente_button_up():
	$click.play()
	if $frames.frame < 2:
		$frames.frame += 1
		return
	$siguiente.disabled=true
	$anim.play("cerrar")
	$bgm.stop()
	yield($anim,"animation_finished")
	get_tree().change_scene_to(siguiente)
