extends Node2D


var velocidad = Player.velocidad
var tengo_item = false
var puntos = {
	"arriba" : null,
	"abajo" : null,
	"izquierda":null,
	"derecha":null
}

var dir_posibles = []
var puntos_posibles = puntos.duplicate()
var tween = Tween.new()
var puedo_mover = true

func _ready():
	add_child(tween)
	
	yield(get_tree(),"idle_frame")
	cargar_direcciones()

func _physics_process(delta):
	
	
	if Input.is_action_pressed("arriba") and puntos_posibles["arriba"]!=null:
			mover(puntos_posibles["arriba"].global_position)
	
	elif Input.is_action_pressed("abajo") and puntos_posibles["abajo"]!=null:
			mover(puntos_posibles["abajo"].global_position)
	
	elif Input.is_action_pressed("izquierda") and puntos_posibles["izquierda"]!=null:
			mover( puntos_posibles["izquierda"].global_position )
	
	elif Input.is_action_pressed("derecha") and puntos_posibles["derecha"]!=null:
			mover( puntos_posibles["derecha"].global_position )

func cargar_direcciones():
	puntos_posibles = puntos.duplicate()
	dir_posibles = []
	
	for dir in $detector.get_children():
		if dir.is_colliding():
			dir_posibles.append(dir.name)
			puntos_posibles[dir.name] = dir.get_collider()
#			print(puntos_posibles[dir.name].name)
			
	
	print(dir_posibles)

func mover(pos):
	if not puedo_mover:
		return
	
	Sfx.play("movimiento")
	
	puedo_mover = false
	
	tween.interpolate_property(self, "global_position",
			global_position, pos, global_position.distance_to(pos) / velocidad,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
	yield(tween,"tween_completed")
	yield(get_tree(),"idle_frame")
	cargar_direcciones()
	puedo_mover = true


